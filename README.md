# Python Scriptlerim

### Scriptler için gereken kütüphaneleri Terminale veya Komut satırına aşağıdaki komutu vererek indirebilirsiniz
```bash
pip install -r requirements.txt
```

### 1. Twitter Çekiliş
* Twitterdan istenilen hashtag'e göre atılan tweetleri çekip, tweetleri atan kullanıcılarla çekiliş yapmaya yarayan yazılım.
* Twython kütüphanesi kullanıldı.

### 2. Kim milyoner olmak ister ?
* sorular.txt adlı dosya içinden rastgele soru çekerek çalışan bir yarışma scripti.
* Birçok eksik özellik mevcut zamanla eklenecek.

### 3. Telegram Botu
* Telegramda BotFather ile oluşturulan botu API_KEY yardımıyla kontrol etmeye yarayan script.
* python-telegram-bot kütüphanesi kullanıldı.
* Detaylar TelegramBOT isimli klasörde mevcut.

### 4. NTV Haber sitesinden haber çekme
* NTV Haber sitesinden Python dosyasında girilen kategoriye göre haber çeker ve kategori isminde klasör oluşturup haberleri isimleri ile o klasöre kaydeder.
* BeatifulSoup ve Urllib kütüphaneleri kullanıldı.

### 5. Anime Notifier
* dizimob adlı internet sitesinden, metin belgesine yazılan animenin yeni bölümü var mı diye kontrol eder. Yeni bölüm varsa pushetta adlı mobil bildirim uygulaması ile kullanıcıya bildirim gönderir.
* BeautifulSoup4 ve Pushetta kütüphaneleri kullanıldı.
